#!/usr/bin/env python

# list files in d

import glob
import csv
from collections import defaultdict
import pprint
# get list of csv files
basepath = '/home/ikhthiandor/testube/THESIS_42/'
csvpath = basepath + '2010_result_csv/'
csv_files = glob.glob( '/home/ikhthiandor/testube/THESIS_42/2010_result_csv/*.csv')
# print(csv_files)

def print_csv_rows(filename):
    with open(filename, 'r') as csvfile:
        print("Filename", filename)
        csvreader = csv.reader(csvfile)
        
        for row in csvreader:
            print(row)
def remove_substring(str_to_replace):
    repls = ('/home/ikhthiandor/testube/THESIS_42/2010_result_csv/2010_result_', ''), ('.csv', '')
    
    return reduce(lambda a, kv: a.replace(*kv), repls, str_to_replace)
    
primary_dict = defaultdict(dict)
# get course codes
for cf in csv_files:
    course_code = remove_substring(cf)
    # print(course_code)
    
    

    with open(cf, 'r') as csvfile:
        # print("Filename", cf)
        csvreader = csv.reader(csvfile)
        
        for row in csvreader:
            
            # {
            # {reg_no1: {course1: grade1, course2: grade2, ...}}
            # {reg_no2: {course1: grade1, course2: grade2, ...}}
            # }
            reg_no = row[0]
            grade = row[1]
            
            primary_dict[reg_no].update({course_code: grade})

pprint.pprint(len(primary_dict['2010331043']))


# create dicts

