import pydot

menu = {'dinner':
            {'chicken':'good',
             'beef':'average',
             'vegetarian':{
                   'tofu':'good',
                   'salad':{
                            'caeser':'bad',
                            'italian':'average'}
                   },
             'pork':'bad'}
        }


lenses_dict = {'tearRate': {'reduced': 'no lenses', 'normal': {'astigmatic': {'yes': {'prescription': {'hyper': {'age': {'pre': 'no lenses', 'presbyopic': 'no lenses', 'young': 'hard'}}, 'myope': 'hard'}}, 'no': {'age': {'pre': 'soft', 'presbyopic': {'prescription': {'hyper': 'soft', 'myope': 'no lenses'}}, 'young': 'soft'}}}}}}

results_dict = {'age': {'2010331068': 'C+', '2010331069': 'C', '2010331035': 'B', '2010331044': 'A', '2010331060': 'A-', '2010331062': 'F', '2010331063': 'F', '2010331064': 'C', '2010331065': 'F', '2010331067': 'B+', '2010331046': 'C', '2010331002': 'C', '2010331008': 'C', '2010331009': 'C-', '2010331042': 'F', '2010331029': 'F', '2010331040': 'C+', '2010331003': 'C+', '2010331024': 'C', '2010331025': 'F', '2010331026': 'F', '2010331027': 'F', '2010331020': 'C+', '2010331021': 'A-', '2010331022': 'B+', '2010331005': 'F', '2010331001': 'B+', '2010331006': 'F', '2010331015': 'A-', '2010331045': 'B', '2009331014': 'F', '2010331072': '', '2010331071': 'B+', '2010331070': 'F', '2010331033': 'C', '2010331018': 'B-', '2010331057': 'B', '2010331056': 'A-', '2010331037': 'B', '2010331036': 'C-', '2010331053': 'F', '2010331034': 'B-', '2010331011': 'B+', '2010331010': 'A-', '2010331013': 'C+', '2010331038': 'C+', '2010331059': 'C+', '2010331017': 'B-', '2010331016': 'F', '2010331055': 'F', '2010331032': 'B+', '2010331031': 'B+', '2010331012': 'F', '2010331030': 'C+', '2010331039': 'B', '2010331051': 'B+', '2010331047': 'B', '2010331043': 'F'}}


def draw(parent_name, child_name):
    edge = pydot.Edge(parent_name, child_name)
    graph.add_edge(edge)

def visit(node, parent=None):
    for k,v in node.iteritems():
        if isinstance(v, dict):
            # We start with the root node whose parent is None
            # we don't want to graph the None node
            if parent:
                draw(parent, k)
            visit(v, k)
        else:
            draw(parent, k)
            # drawing the label using a distinct name
            draw(k, k+'_'+v)

graph = pydot.Dot(graph_type='graph')
visit(results_dict)
graph.write_png('results_graph.png')
