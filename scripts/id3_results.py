# Create an ID3 from results

import math

def csv_to_list(filepath):
     with open(filepath, 'rb') as f:
          reader = csv.reader(filepath)
          your_list = list(reader)
          return your_list
     

def loadDataSet(filepath):
    """
    Loads data set as a list of lists

    Args:
        filepath: Path to data file in TSV format

     Returns:
         Data set as list of lists
     """
    with open(filepath, 'r') as infile:
        return [line.strip().split('\t') for line in infile]


def calcShannonEntropy(dataSet):
    """
    Calculates Shannon entropy of classifications in data

    Args:
        dataSet: Data set

    Return:
        Entropy of data set
    """
    total = float(len(dataSet))
    labels = {}
    for data in dataSet:
        if data[-1] not in labels:
            labels[data[-1]] = 0.0
        labels[data[-1]] += 1
    entropy = 0
    for count in labels.values():
        entropy -= (count / total) * math.log(count / total, 2)
    return entropy


def splitDataSet(dataSet, axis, value):
    """
    Splits data set on a given column containing a specified value.

    Returns the data set minus the specified column.

    Args:
        dataSet: Data set
        axis: Column of value to split on
        value: Value to split on

    Returns:
        Data set split by value and minus the specified column.
    """
    subSet = []
    for data in dataSet:
        if data[axis] == value:
            subSet.append(data[:axis] + data[axis + 1:])
    return subSet


def chooseBestFeatureToSplit(dataSet):
    """
    Split on the feature with the largest information gain.

    Args:
        dataSet: Data set

    Returns:
        Column index of best feature
    """
    total = float(len(dataSet))
    features = [{} for i in xrange(len(dataSet[0][:-1]))]
    baseGain = calcShannonEntropy(dataSet)
    infoGain = [0] * len(features)

    for data in dataSet:
        result = data[-1]
        for i, feature in enumerate(data[:-1]):
            if feature not in features[i]:
                features[i][feature] = []
            features[i][feature].append((feature, result))

    for i, feature in enumerate(features):
        entropy = 0
        for branch in feature.values():
            entropy += len(branch) / total * calcShannonEntropy(branch)
        infoGain[i] = baseGain - entropy

    maxGain = max(infoGain)
    return infoGain.index(maxGain)


def createTree(dataSet, labels):
    """
    Creates decision tree using ID3 algorithm

    Args:
        dataSet: Data set
        labels: Labels of features/columns

   Returns:
       Decision tree to use for classification
   """


    classifications = [example[-1] for example in dataSet]

    if all(classifications[0] == classification for classification in classifications):
        return classifications[0]

    if len(dataSet[0]) == 1:
        return max(set(classifications), key=classifications.count)

    bestFeature = chooseBestFeatureToSplit(dataSet)
    tree = {labels[bestFeature]: {}}
    for value in set([example[bestFeature] for example in dataSet]):
        subset = splitDataSet(dataSet, bestFeature, value)
        tree[labels[bestFeature]][value] = createTree(
        subset, labels[:bestFeature] + labels[bestFeature + 1:])
    return tree


def classify(inputData, tree, labels):
    """
    Classify given data using decision tree

    Args:
        inputData: Input data to classify
        tree: Decision tree
        labels: Labels for features/columns
    """
    if not isinstance(tree, dict):
        return tree
    label = tree.keys()[0]
    labelIndex = labels.index(label)
    return classify(inputData, tree[label][inputData[labelIndex]], labels)


def main():
    filepath = 'lenses.txt'
    labels = ['age', 'prescription', 'astigmatic', 'tearRate']
    dataSet = loadDataSet(filepath)
    tree = createTree(dataSet, labels)
    print tree


if __name__ == '__main__':
    main()
